This folder contains some ERP analysis functions of mine that are used in the [Neurolinguistics Lab](http://msuneuroling.weebly.com) in the [Department of Linguistics and Languages](http://linglang.msu.edu) at Michigan State University.

It also contains some some PCA functions that are still under construction. These don't work as intended right now.

Karthik Durvasula.