#This script is incomplete


#Reading ERP Data
Data=read.csv("/Users/karthikdurvasula/Dropbox/3. Underspec MMN/voltage data/1.ba-voltage.csv")
#Data[1:10,]
#levels(factor(Data$segm_time))
#nrow(Data)/4/250


#*************
#Plotting and checking for Normality
library(ggplot2)
#Plotting the Data for Cz
ggplot(Data, aes(x=Cz,col=factor(segm_time)))+geom_density()+facet_wrap(~CONDITION*PHONEME)

#plotting only part of the data
Data2=Data[Data$segm_time>0 & Data$segm_time < 400,]
ggplot(Data2, aes(x=Cz,col=factor(segm_time)))+geom_density()+facet_wrap(~CONDITION*PHONEME)

#*************
#PCA Analysis for the data
#names(Data[,38:167])
pc1 = princomp(Data[,40:168])
predict(pc1)